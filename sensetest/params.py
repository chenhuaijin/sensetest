from dataclasses import dataclass


@dataclass
class Params:

    # Output
    save: str = ""
    save_format: str = "json"
    precision: int = 5

    # Module-level switch flags
    dehaze: bool = True
    denoise: bool = True
    quality: bool = True
    imatest: bool = True

    # Dehaze switches
    accutance: bool = True
    contrast: str = "RMS"

    # Quality Metrics
    PSNR: bool = True
    MSSSIM: bool = True

    # Benchmarking
    night_scene: str = ""
    normal_scene: str = ""
    strong_backlit_scene: str = ""
