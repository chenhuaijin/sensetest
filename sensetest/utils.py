import os
from typing import Union

import cv2
import numpy as np
import torch


def validate_np(img: Union[torch.Tensor, np.ndarray]) -> np.ndarray:
    """C,H,w -> H, W, C if torch"""
    if isinstance(img, np.ndarray):
        return img
    return img.cpu().permute(1, 2, 0).numpy()


def validate_torch(img: Union[torch.Tensor, np.ndarray]) -> torch.tensor:
    """H,W,C -> C,H,W if np"""
    if isinstance(img, torch.Tensor):
        return img
    return torch.from_numpy(img).permute(2, 0, 1).unsqueeze(0)
