import cv2
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


# For a definition of accutance see:
# https://en.wikipedia.org/wiki/Acutance
class Accutance(nn.Module):
    def __init__(self, device, channels: int = 3, eps: float = 0.005):

        super(Accutance, self).__init__()

        self.kernel_a = (
            torch.from_numpy(np.array([[1, 0, -1], [2, 0, -2], [1, 0, -1]])).float().unsqueeze(0)
        ).to(device)
        self.kernel_a = self.kernel_a.repeat(channels, 1, 1, 1)

        self.kernel_b = (
            torch.from_numpy(np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]])).float().unsqueeze(0)
        ).to(device)
        self.kernel_b = self.kernel_b.repeat(channels, 1, 1, 1)

        self.eps = torch.tensor(eps).double().to(device)

    def forward(self, x: torch.Tensor):
        """
        x: Image to calculate accutance for
        Shape (H, W), (C, H, W) or (N, C, H, W)
        Range [0, 1] np.float32
        """

        G_x = F.conv2d(x, self.kernel_a, groups=self.kernel_a.shape[0])
        G_x = torch.where(G_x < self.eps, self.eps, G_x.double())

        G_y = F.conv2d(x, self.kernel_b, groups=self.kernel_b.shape[0])
        G_y = torch.where(G_y < self.eps, self.eps, G_y.double())

        G = torch.sqrt(G_x ** 2 + G_y ** 2)

        return G.mean()


### Contrast Metrics
### https://en.wikipedia.org/wiki/Contrast_(vision)

##The following operations use luminance values of the image
## to do RGB -> LAB they expect a numpy array


class Contrast(nn.Module):
    def __init__(self, mode: str = "RMS"):
        super(Contrast, self).__init__()

        self.mode = mode

    def forward(self, x: np.ndarray) -> float:
        """
        x: [0, 1] RGB Space
        """
        # Convert RGB -> LAB
        x = cv2.cvtColor(x, cv2.COLOR_RGB2LAB)
        L = x[:, :, 0]

        if self.mode == "michelson":
            contrast = (L.max() - L.min()) / (L.max() + L.min())
        elif self.mode == "RMS":
            contrast = np.std(L)

        return contrast
