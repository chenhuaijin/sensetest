import base64
from dataclasses import dataclass
import logging
import os
import socket
import subprocess
import sys

import paramiko

logger = logging.getLogger("root")

HOSTNAME = "10.52.4.52"
USERNAME = "senseftp"
PWD = "Sensebrain@2020"


@dataclass
class ImatestProxy:

    HOSTNAME = "10.52.4.52"
    USERNAME = "senseftp"
    PWD = "Sensebrain@2020"
    IMATEST_PATH = "/home/senseftp/UDC_IQ_Metrics"
    # Internal Variables managing the connection
    _client = None
    _sftp = None

    def __post_init__(self):
        self.load_connection()

    def load_connection(self):
        """Load client and sftp"""
        try:
            self._client = paramiko.SSHClient()
            self._client.load_system_host_keys()
            self._client.connect(
                self.HOSTNAME, username=self.USERNAME, password=self.PWD, timeout=15
            )
            self._sftp = self._client.open_sftp()
        except socket.timeout:
            raise Exception(
                "Imatest server connection timed out, please check your US VPN connection"
            )

    @property
    def client(self):
        if self._client:
            return self._client
        self.load_connection()
        return self._client

    @property
    def sftp(self):
        if self._sftp:
            return self._sftp
        self.load_connection()
        return self._sftp

    def close(self):
        if self._client:
            self._client.close()
            self._client = None
            self._sftp = None

    def calc_metrics(self, chart: str, input_path: str, output_path: str):
        """
        input_path: path to images that have been run
        output_path: where to save all the output from imatest
        c: "color-checker" or "TE-42"
        """
        print(f"STARTING IMATEST AUTOMATED TESTING on SERVER {self.HOSTNAME}")
        # 1: Create temp dir for running IMATEST on server
        command = f"cd {self.IMATEST_PATH} && mkdir tmp/"
        self.REMOTE_IMAGE_PATH = f"{self.IMATEST_PATH}/tmp"
        self.client.exec_command(command)

        # 2: send images to server
        print(f"SENDING IMAGES TO {self.HOSTNAME}")
        dirlist = os.listdir(input_path)
        for elem in dirlist:
            img_path = f"{input_path}/{elem}"
            self.sftp.put(img_path, f"{self.REMOTE_IMAGE_PATH}/{elem}")
        print(f"FINISHED SENDING IMAGES TO {self.HOSTNAME}")

        # 2: Send command to server
        print(f"EXECUTING IMATEST COMMAND ON REMOTE SERVER")
        metric = "YSNR" if chart == "color-checker" else "MTF"
        command = f"cd {self.IMATEST_PATH} && ./run_imatest_IT.py {self.REMOTE_IMAGE_PATH} YSNR"
        stdin, stdout, stderr = self.client.exec_command(command)
        for line in stdout:
            print(line)

        # 3: Get images from server
        logger.debug("FETCHING IMATEST RESULTS")
        os.makedirs(output_path, exist_ok=True)
        remote_path = f"{self.REMOTE_IMAGE_PATH}/Results"
        dirlist = self.sftp.listdir(remote_path)
        for elem in dirlist:
            elem_path = f"{remote_path}/{elem}"
            self.sftp.get(elem_path, f"{output_path}/{elem}")

        # 4: Create temp dir for running IMATEST on server
        command = f"cd {self.IMATEST_PATH} && rm -rf tmp"
        self.client.exec_command(command)

        self.close()
        logging.info(f"CLOSED CONNECTION TO REMOTE IMATEST SERVER {self.HOSTNAME}")


def send_command():
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.connect(HOSTNAME, username=USERNAME, password=PWD)
    # stdin, stdout, stderr = client.exec_command('ls')
    stdin, stdout, stderr = client.exec_command(
        "cd Imatest_IQ_Metrics && ./run_imatest_IT.py UDC_test/ YSNR"
    )
    for line in stdout:
        print("... " + line.strip("\n"))
    client.close()


def get_files():
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.connect(HOSTNAME, username=USERNAME, password=PWD)
    sftp = client.open_sftp()

    path = "Imatest_IQ_Metrics/UDC_test/Results"
    dirlist = sftp.listdir(path)
    for elem in dirlist:
        abspath = f"{path}/{elem}"
        sftp.get(abspath, f"{elem}")

    client.close()


if __name__ == "__main__":
    proxy_server = ImatestProxy()
    proxy_server.calc_metrics("data/IQ/XM", "_reports/tst")
