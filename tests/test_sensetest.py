import os
import sys

sys.path.append(".")

import cv2
import numpy as np

from sensetest.sensetest import ImTest

if __name__ == "__main__":

    imtest = ImTest(name="Testing 001")

    test_dir = "data/IQ/XM"
    for f in os.listdir(test_dir):
        path = f"{test_dir}/{f}"
        rgb_image = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGB)
        rgb_image = rgb_image.astype(np.float32) / 255
        blurred = cv2.GaussianBlur(rgb_image, (15, 15), 5)
        imtest.collect(
            name=f.split(".")[0],
            img_in=rgb_image,
            img_out=blurred,
        )

    imtest.run_imatest(chart="color-checker", metrics=["YSNR"])
    metrics = imtest.close(save=True)
    print(metrics)
