## Requirements
See requirements.txt for repo requirements

To run imatest metrics you need to have VPN permission access the server, ask Ning Jiang to grant you access to 10.52.4.52.
You need to be connected to the US VPN to run the imatest metrics.

## Installation
1. Git Clone source repository
2. Pip install from source
```
cd sensetest
pip install .
```

## Sample Usage
See tests/ for reference usage of the sensetest repo

## Example JSON output
```json
{
    'imgs': {
        '20201114_143927980': {
            'msssim': 0.0,
            'psnr': 0.0,
            'accutance_out': 0.032682076033921884,
            'accutance_in': 0.08018580331524955,
            'accutance_ref': 0.0,
            'contrast_out': 23.134373,
            'contrast_in': 24.124079,
            'contrast_ref': 0.0,
            'delta_E': [7.81, 7.14, 8.83, 9.05, 8.49, 7.45],
            'YSNR': [39.44, 39.37, 39.09, 39.52]
        },
        '20201125_153831540': {
            'msssim': 0.0,
            'psnr': 0.0,
            'accutance_out': 0.03692214433452282,
            'accutance_in': 0.1001094965361845,
            'accutance_ref': 0.0,
            'contrast_out': 22.612762,
            'contrast_in': 23.650537,
            'contrast_ref': 0.0,
            'delta_E': [23.7, 15.6, 13.6, 15.1, 15.9, 10.5],
            'YSNR': [34.84, 35.21, 34.95, 35.42]
        }
    },
    'mean': {
        'msssim': 0.0,
        'psnr': 0.0,
        'accutance_out': 0.02320140678948157,
        'accutance_in': 0.06009843328381135,
        'accutance_ref': 0.0,
        'contrast_out': 15.249045054117838,
        'contrast_in': 15.924872080485025,
        'contrast_ref': 0.0
    },
    'count': 2,
    'name': 'Testing 001',
    'meta': {}
}
```


